##### Makefile #####
CC=g++
CPPFLAGS=-Wall -Wconversion -std=c++1y

all: pkmn

pokemao: main_take.o pokemao.o
	$(CC) $(CPPFLAGS) -o pokemao main.o pokemao.o

main_take.o: main.cpp
	$(CC) $(CPPFLAGS) -o main.o -c main.cpp

pokemao.o: pokemao.cpp pokemao.h
	$(CC) $(CPPFLAGS) -o pokemao.o -c pokemao.cpp

aux.o: aux.hpp aux.cpp
	$(CC) $(CPPFLAGS) -c aux.cpp -o aux.o

main.o: main.cpp
	$(CC) $(CPPFLAGS) -c main.cpp -o main.o

pkmn: aux.o main.o
	$(CC) $(CPPFLAGS) aux.o main.o -o pkmn.out

clean:
	rm -rf *.o*

redo: clean all

mrproper: clean
	rm -rf pokemao
