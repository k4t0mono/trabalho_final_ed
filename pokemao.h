/*
 * Trabalho final da disciplina Estrutura de Dados
 * Arvore B+ - Pokemons
 * Copyright 2017 by Rafael Yukio Takehara Carvalho & Frederico & Breno
 * Arquivo ".h" onde são declaradas as classes e funções
*/
#ifndef POKEMAO_POKEMAO_H
#define POKEMAO_POKEMAO_H

#include <iostream>
#include <cstring>
#include <sstream>

using namespace std;

const int TAM_BLOCO_SEQUENCE_SET = 4;
const int ORDEM = 5;

//excecao
enum TipoExcecao {ArvoreVazia, ValorNaoPresente};

class Excecao {
private:
    //dados
    int Linha;
    TipoExcecao Tipo;
    //dados
public:
    //metodos
    Excecao(TipoExcecao tipo, int linha);
    string Msg();
    //metodos
};
//excecao


struct pokemon {
    int id;
    char nome[50]; //nome do pokemon
    char tipo1[10]; //tipo principal do pokemon
    char tipo2[10]; //tipo secundario do pokemon
    float altura; //altura do pokemon
    float peso; //peso do pokemon

    pokemon(int idP, char *nomeP, char *tipo1P, char *tipo2P, float alturaP, float pesoP);
    ~pokemon();
    // std::istream& operator>>(std::istream& is, pokemon& pk);
    // std::ostream& operator<<(std::ostream& os, pokemon& pk);
};


// O que vai ser escrito no disco
class bloco {
private:
    pokemon dados[TAM_BLOCO_SEQUENCE_SET]; //Objetos no bloco
    int numPokemons; //Número de pokemons no bloco
    int prox; //RRN do próximo bloco
public:
    bloco();
};

// O que fica na memoria
class noh  {
private:
    int indice[ORDEM-1]; // valores que separam os blocos
    bool folha; // se é folha ou não da árvore-B
    noh* filhos; //ponteiros para os filhos (se nó interno)
    int blocos[ORDEM]; //RRNs dos blocos (se for nó folha)
    int numIndices; //número de chaves no nó
public:
    noh(bool folhaP);
    ~noh();

friend class bPlus;
};


struct cabecalhoArquivo {
    int nroBlocos;
    int posRaiz;
    int posBlocoInvalida;
};

class bPlus {
private:
    cabecalhoArquivo cabecalho;
    // string nomeArqBlocos; //possibilidade de ponteiro pro arquivo
    // string nomeArqIndex; //ponteiro do noh* filhos da classe noh me fode

    std::fstream* arquivo;
    noh* raiz;

    void lerBlocoArquivo(bloco* b, int posDisk);
    void escreverBlocoArquivo(bloco* b, int posDisk);
//    void divideBloco(pagina* p, int posAtual, pilha* ancestraisPagina);
//    int proxPosArqUtilizar(); // próxima posição no disco a ser utilizada
public:
    bPlus(char* nomeArq);
    ~bPlus();

    void insere();
    void remove();
    void busca();
    void imprimeBlocosOrdenados();
    void imprimePorAltura();

    void verificaEstoro();
    void verificaAbaixo();

    void atualizaIndice();
    void removeIndice();

    void separaNoh();
    void separaBloco();
    void juntaNoh();
    void juntaBloco();

    void distribui();


    /*extra*/ void ordenarBlocos(); //depois de X operacoes, ordena os blocos no arquivo
};



#endif //POKEMAO_POKEMAO_H
