#ifndef DADO_HPP
#define DADO_HPP

#include <iostream>
#include <cstdlib>
#include <cstring>

using namespace std;

// geração de dados aleatórios
static const char alpha[] = 
"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
"abcdefghijklmnopqrstuvwxyz";

static const char alphanum[] =
"0123456789"
"!@#$%^&*"
"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
"abcdefghijklmnopqrstuvwxyz";

const int alphaLength = sizeof(alpha) - 1;
const int alphaNumLength = sizeof(alphanum) - 1;

// geradores aleatórios de caracteres
char geraAlpha(){
    return alpha[rand() % alphaLength];
}

char geraAlphaNum(){
    return alphanum[rand() % alphaNumLength];
}

class Dado {
friend class arvoreB;    
private: 
    int chave1;
    int chave2;
    float valor1;
    char campo1[5];
    char campo2[20];
    char lixo[500];
public:
    void geraAletorio(int k=-1);
    void imprime(bool imprimeLixo = false);
};

void Dado::geraAletorio(int k) {
    chave1 = k;
    // preenchendo outros dados aleatoriamente
    chave2 = rand();
    // gera um número entre 0 e 1
    valor1 = ((float) rand()) / ((float) RAND_MAX);
    // gerando as strings, garantido caracter na primeira posição
    campo1[0] = geraAlpha();
    campo2[0] = geraAlpha();    
    for (int i = 1; i < 5; i++) {
        campo1[i] = geraAlphaNum();
    }
    for (int i = 1; i < 20; i++) {
        campo2[i] = geraAlphaNum();
    }
    
    // gerando 500 bytes aleatórios
    for (int i = 0; i < 499; i++) {
        lixo[i] = rand();
    } 
}

void Dado::imprime(bool imprimeLixo) {
    // não podemos imprimir campo1 e campo2 (e lixo) diretamente
    // porque eles não tem o caracter de final de string ('\0')
    // criamos três strings auxiliares para fazer a impressão
    char campo1aux[6];
    char campo2aux[21];
    char lixoaux[501];
    
    // não posso usar strcpy ou copiar para string pq campo1, campo2
    // não possuem caracter de término de string, precisa fazer um for
    // ou usar strncpy
    strncpy(campo1aux, campo1, 5);
    strncpy(campo2aux, campo2, 20);
    strncpy(lixoaux, lixo, 500);

    campo1aux[5] = '\0';
    campo2aux[20] = '\0';
    lixoaux[500] = '\0';
    
    cout << chave1 << " " << chave2 << " " 
         << valor1 << endl
         << campo1aux << " " << campo2aux << endl;
         
    if (imprimeLixo) {
        cout << lixo << endl;
    } 
    cout << endl << endl;
}

#endif
