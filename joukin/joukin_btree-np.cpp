// Árvore B em disco
// Estruturas de Dados - 2016/1
// Código by Joukim

#include <iostream>
#include <fstream>
#include "dado.hpp"
#include "pilha.hpp"

using namespace std;

/* Para esta implementação, adotou-se a seguinte definição de grau, 
 * retirada de :
 * 
 * 
 * Em minha opinião, essa definição é uma das mais claras e de mais 
 * fácil implementação: o grau de uma árvore B é o menor número de 
 * chaves (dados) que um nó (uma página pode conter). Ela é mais fácil,
 * porque quando a página "estoura", possui um número ímpar de chaves,
 * permitindo facilmente obter o elemento central.
 * 
 * Definições alternativas de grau:
 * 
 * I. Wikipedia: Usually, the number of keys is chosen to vary between 
 * d and 2d, where d is the minimum number of keys, 
 * and d+1 is the minimum degree or branching factor of the tree. 
 * 
 * II. Cormen: Nodes have lower and upper bounds on the number 
 * of keys they can contain. We express these bounds in terms of a 
 * ﬁxed integer t called the minimum degree of the B-tree: 
 * a. Every node other than the root must have at least t - 1 keys. 
 * (...) Every node may contain at most 2t - 1 keys. 
 * 
 * III. Knuth: According to Knuth's definition, a B-tree of order m 
 * is a tree which satisfies the following properties:
 * Every node has at most m children.
 * Every non-leaf node (except root) has at least ⌈m/2⌉ children.
 * 
 * IV. Aho & Hopcroft: Formally, a B-tree of order m is an m-ary 
 * search tree with the following properties: 
 * 1. The root is either a leaf or has at least two children. 
 * 2. Each node, except for the root and the leaves, has between [m/2] 
 * and m children. 
 * 
 * V. Horvick: More formally, each non-root node in the B-tree will 
 * contain at least T children and a maximum of 2 × T children. 
 * (...) Every non-root node will have at least T - 1 values.
 * Every non-root node will have at most 2 × T - 1 values.
 * 
 * VI. Sedgewick: B tree of order M is a tree that either is empty or 
 * comprises k-nodes, with k-1 keys and k links (...) k must be between 
 * 2 and M at the root and between M/2 and M at every other node (...).
 * 
*/

// cabeçalho da árvore B
struct cabArqB {
    int grau; // grau M -> entre M e 2M chaves
    int nPaginas;
    int posRaiz;
    int pagInvalida;  // primeira página inválida
};

class pagina {
friend class arvoreB;
private:
    char folha;     // 1 -> folha --- 0 -> não-folha
    int num;        // número de chaves (objetos) armazenadas na página
    int* posFilhas; // até 2M+1 filhas (posFilhas -> posição relativa no arquivo)
    Dado* objetos;  // até 2M objetos/chaves (alocado com 2M+1 para permitir estouro)
public:
    pagina(unsigned M, char f = 1); 
};

pagina::pagina(unsigned M, char f) {
    folha = f;
    num = 0;
    posFilhas = new int[2*M+1];
    objetos = new Dado[2*M+1]; // um a mais para permitir estouro
}

class arvoreB {
private:
    cabArqB cabecalho;
    string nomeArq;
    pagina* raiz;
//    void dividePagFilha(pagina* P, int posFilha);
//    void insereEmPagNaoCheia(pagina* P, int chave);
    void percorreEmOrdemAux(pagina* P, int nivel);
    void lerPagArquivo(pagina* p, int posDisk);
    void escreverPagArquivo(pagina* p, int posDisk);   
    void dividePagina(pagina* p, int posAtual, pilha* ancestraisPagina);
    int proxPosArqUtilizar(); // próxima posição no disco a ser utilizada
public:
    arvoreB(string nome, int grau=2);
    ~arvoreB(); // precisa gravar o cabeçalho!!!!
    void percorreEmOrdem( );
    pagina* busca(int chave);
    void insere(Dado& umDado);
};


arvoreB::arvoreB(string nome, int grau) {
    nomeArq = nome;
    ifstream arqE(nomeArq.c_str(), ios::binary);

    if ( arqE ) {
        arqE.read((char*) &cabecalho, sizeof(cabArqB));
        if (cabecalho.nPaginas == 0) {
            raiz = NULL;
        } else { 
            raiz = new pagina(cabecalho.grau);
            lerPagArquivo(raiz, cabecalho.posRaiz);
        }
        arqE.close();
    
    } else { // não existe o arquivo de entrada ainda, nova árvore
        ofstream arqS(nomeArq.c_str(),ios::out | ios::binary);
        raiz = NULL;
        cabecalho.grau = grau;
        cabecalho.nPaginas = 0;
        cabecalho.pagInvalida = -1;
        arqS.write((const char*) &cabecalho, sizeof(cabArqB));
        arqS.close();
    } 
}


void arvoreB::lerPagArquivo(pagina* p, int posPag){
    ifstream arqE(nomeArq.c_str(), ios::binary);
    int M = cabecalho.grau;
    
    // num_chaves + eh_folha + 2*M (chaves) + 2*M+1 filhos
    // obs: em memória temos 2*M + 1 chaves, para permitir estouro,
    //      mas não no arquivo
    int tamPag =  sizeof(char) + sizeof(int)  
                 + ( sizeof(int) * ((2 * M) + 1) )
                 + ( sizeof(Dado) * (2 * M) );
    
    // pula o cabeçalho do arquivo e o número de páginas anteriores             
    int posArq = sizeof(cabecalho) + posPag*tamPag;
    
    // leitura dos dados
    arqE.seekg(posArq);
    arqE.read((char*) &(p->folha),sizeof(char));
    arqE.read((char*) &(p->num),sizeof(int));
    // lendo as posições das páginas filhas
    arqE.read((char*) p->posFilhas,sizeof(int)*((2*M) + 1));     
    // lendo n dados do vetor em sequencia
    // não precisa ler tudo, para não perder tempo com lixo
    arqE.read((char*) p->objetos,sizeof(Dado)*(p->num));    

    arqE.close();  
}


void arvoreB::escreverPagArquivo(pagina* p, int posPag){
    // usando fstream para não sobrescrever o arquivo
    fstream arqS(nomeArq.c_str(), ios::in | ios::out | ios::binary);
    
    int M = cabecalho.grau;
    
    // num_chaves + eh_folha + 2*M (chaves) + 2*M+1 filhos
    // obs: em memória temos 2*M + 1 chaves, para permitir estouro,
    //      mas não no arquivo
    int tamPag = sizeof(char) + sizeof(int)   
                 + ( sizeof(int) * ((2 * M) + 1) )
                 + ( sizeof(Dado) * (2 * M) );
    
    
    // pula o cabeçalho do arquivo e o número de páginas anteriores             
    int posArq = sizeof(cabecalho) + posPag*tamPag;
    
    // escrita dos dados
    arqS.seekp(posArq);
    arqS.write((char*) &(p->folha),sizeof(char));
    arqS.write((char*) &(p->num),sizeof(int));
    // escrevendo as posições das páginas filhas
    arqS.write((char*) p->posFilhas,sizeof(int)*((2*M) + 1)); 
    // escrevendo os dados do vetor em sequencia
    // verificar se não precisa escrever lixo no restante do bloco
    // se precisar trocar p->num por 2*M
    arqS.write((char*) p->objetos,sizeof(Dado)*(p->num));    
    // em Linux não precisa:
    /*
      http://man7.org/linux/man-pages/man2/lseek.2.html
      The lseek() function allows the file offset to be set beyond the end
      of the file (but this does not change the size of the file).  If data
      is later written at this point, subsequent reads of the data in the
      gap (a "hole") return null bytes ('\0') until data is actually
      written into the gap.
    */
      
    arqS.close();  
}



void arvoreB::insere(Dado& umDado) {
    int M = cabecalho.grau;
    
    // se árvore estiver vazia, aloca nó folha para a raiz
    // e insere objeto na posição inicial    
    if (raiz == NULL) {
        raiz = new pagina(M);
        raiz->objetos[0] = umDado; 
        raiz->num = 1; 
        
        // alterando o cabeçalho do arquivo
        // usa fstream para não apagar conteúdo do arquivo
        fstream arq(nomeArq.c_str(), ios::in | ios::out | ios::binary);
        cabecalho.posRaiz = 0;
        cabecalho.nPaginas = 1;
        arq.write((const char*) &cabecalho, sizeof(cabArqB)); 
        arq.close(); 

        // escreve a raiz no disco
        // estamos considerando que não há páginas inválidas
        // (árvore remove páginas inválidas ao remover o último elemento)
        escreverPagArquivo(raiz, 0);
    } else { // já tem algo na raiz
        pagina atual(M);
        lerPagArquivo(&atual, cabecalho.posRaiz);
        
        //buscamos a página para inserção (precisa ser folha)
        int posArquivo = cabecalho.posRaiz;

        // caso a página gere estouro, precisamos subir chave ao nó 
        // superior, então armazenamos o caminho percorrido
        pilha ancestraisPaginaAtual;

        while (atual.folha != 1) {
            // procura página filha para fazer a inserção
            int pos = 0;
            while (umDado.chave1  > atual.objetos[pos].chave1) {
                pos++;
            }
            // salva a posição do pai da página atual, antes
            // de carregar a página filha
            ancestraisPaginaAtual.empilha(posArquivo);
            
            // carrega página filha
            posArquivo = atual.posFilhas[pos];
            lerPagArquivo(&atual, posArquivo);   
        }
        
        // estamos em um nó folha, agora é inserir
        // vamos movendo do final para o início
        // até encontrar posição
        int i = atual.num;
        while ( (i > 0) and (atual.objetos[i-1].chave1 > umDado.chave1) ) {
            atual.objetos[i] = atual.objetos[i-1];
            i--;
        }
 
        // Insere novo objeto no local encontrado
        atual.objetos[i] = umDado;
        atual.num++;
        
        if (atual.num > 2*M) {
            // tem que dividir a página
            dividePagina(&atual, posArquivo, &ancestraisPaginaAtual);
            
        } else {
            // basta gravar a página
            escreverPagArquivo(&atual, posArquivo);
        }
    }    
}


void arvoreB::dividePagina(pagina* p, int PosAtual, pilha* ancestraisPagina) {
    int M = cabecalho.grau;
    pagina* atual = p;
    
    while (atual->num > 2*M) {
        // cria página nova no mesmo nível da página atual
        pagina* nova = new pagina(M,atual->folha); 
        int posNova = proxPosArqUtilizar();
        cabecalho.nPaginas++;
        
        // copia chaves e posições de páginas filhas para página nova
        // DESENHAR PARA CONFERIR
        for (int i = 0; i < M; i++) {
            nova->objetos[i] = atual->objetos[M+1 + i];
            nova->posFilhas[i] = atual->posFilhas[M+1 + i];
        }
        
        nova->num = M;
        atual->num = M;
        Dado umDado = atual->objetos[M]; // para subir para página pai
        
        // grava página atual e página nova no disco   
        escreverPagArquivo(atual, posAtual); 
        escreverPagArquivo(nova, posNova); 
        
        // tentamos subir chave do meio para pai da página atual
        pagina* pai = new pagina(M,0);
        int posicaoPai; 
        
        // verifica se existe um pai da página atual
        if (ancestraisPagina->vazia()) { 
            // página atual é raiz, precisa criar nova página e 
            // torná-la nova raiz
            posicaoPai = proxPosArqUtilizar(); 
            cabecalho.nPaginas++;
            raiz = pai;
            cabecalho.posRaiz = posicaoPai;
            pai->posFilhas[0] = posAtual;
            pai->posFilhas[1] = posNova;
            pai->objetos[0] = umDado;
            pai->num = 1;
        } else { // carrega o pai do nó atual
            posicaoPai = ancestraisPagina->desempilha();
            lerPagArquivo(pai, posicaoPai);
            
            // vamos movendo do final para o início
            // até encontrar posição para inserir chave
            int i = pai.num;
            while ( (i > 0) and (pai->objetos[i-1].chave1 > umDado.chave1) ) {
                pai->objetos[i] = pai->objetos[i-1];
                i--;
            } // PRECISA TAMBÉM MOVER OS FILHOS
        }
        // sobe nível na árvore, para continuar conferência do estouro
        atual = pai;
    }
    
}

int arvoreB::proxPosArqUtilizar() {
    if (cabecalho.pagInvalida == -1) {
        return cabecalho.nPaginas;
    } else {
        int posicao = cabecalho.pagInvalida;
        pagina* pagInval = new pagina(M);
        lerPagArquivo(pagInval, posicao);
        cabecalho.pagInvalida = pagInval->posFilhas[0];
        return posicao;
    }  
}

/* else { // já tem algo na raiz
        // caso raiz esteja cheia, cresça árvore em altura
        if (raiz->n == 2*grau-1) {
            pagina* novaP = new pagina(grau, 0);
 
            // torna raiz filho da nova página
            novaP->filhas[0] = raiz;
 
            // Divide raiz antiga e sobe uma chave para nova raiz
            dividePagFilha(novaP, 0);
 
            // Nova raiz tem agora duas páginas filhas...
            // Verifica qual das duas irá receber a chave
            int i = 0;
            if (chave > novaP->chaves[0])
                i++;
            insereEmPagNaoCheia(novaP->filhas[i],chave);
 
            // Change root
            raiz = novaP;
            
            // Gravando modificações no arquivo
            escreverPagArquivo(raiz, cab.posRaiz);
            
            // A nova página é escrita no final do arquivo
            int posNovaPagina =  cab.nPaginas;
            escreverPagArquivo(novaP, posNovaPagina)
            
            // Modifica o cabeçalho
            cab.posRaiz = posNovaPagina;
            cab.nPaginas++;
        }
        else  // se raiz não está cheia, chama insiraNaoCheia nela
            insereEmPagNaoCheia(raiz, chave);
    } 
}

void arvoreB::dividePagFilha(pagina* P, int posFilha) {
    int grau = cab.grau;
        
    // Cria nova página para armazenar metade das chaves 
    // da página filha
    pagina *filha = P->filhas[posFilha];
    pagina *novaP = new pagina(grau, filha->folha); 
    novaP->n = grau - 1;
 
    // Copia os últimas chaves e filhas da página filha para novaP
    for (int j = 0; j < grau-1; j++) {
        novaP->chaves[j] = filha-> chaves[j+grau];
    }
    if (filha->folha == 0) {
        for (int j = 0; j < grau; j++)
            novaP->filhas[j] = filha->filhas[j+grau];
    }

    // Reduz número de chaves na página filha
    filha->n = grau - 1;
 
    // Cria espaço no vetor de filhas do nó atual 
    // para inserir novaP
    for (int j = P->n; j >= posFilha+1; j--) {
        P->filhas[j+1] = P->filhas[j];
    }
 
    // Insere apontador para novaP no nó atual
    P->filhas[posFilha+1] = novaP;
 
    // Cria espaço no vetor de chaves para a chave que
    // subirá da página filha
    for (int j = P->n-1; j >= posFilha; j--) {
        P->chaves[j+1] = P->chaves[j];
    }
 
    // Copia chave do meio da página filha para nó atual
    P->chaves[posFilha] = filha->chaves[grau-1];
    P->n = P->n + 1;
}


void arvoreB::insereEmPagNaoCheia(pagina* P, int chave) {
    int grau = cab.grau;
    
    // inicializa índice como elemento mais à direita
    int i = P->n-1;
 
    // Caso página atual seja folha, 
    // encontre o local para inserir nova chave 
    // e mova outros valores uma posição à frente
    if (P->folha == 1) {
        while (i >= 0 && P->chaves[i] > chave) {
            P->chaves[i+1] = P->chaves[i];
            i--;
        }
 
        // Insere nova chave no local encontrado
        P->chaves[i+1] = chave;
        P->n = P->n+1;
    } else { // nó não é folha
        // Encontra página filha que irá receber a nova chave
        while (i >= 0 && P->chaves[i] > chave) {
            i--;
        }
 
        // Verifica se página encontrada não está cheia
        if (P->filhas[i+1]->n == 2*grau-1) {
            // Se página filha está cheia, divida-a
            dividePagFilha(P, i+1);
 
            // Após divisão, verifica qual das duas partes
            // irá receber a nova chave
            if (P->chaves[i+1] < chave)
                i++;
        }
        insereEmPagNaoCheia(P->filhas[i+1], chave);
    }
}
*/


void arvoreB::percorreEmOrdem( ){
    percorreEmOrdemAux(raiz,0);    
    cout << endl;
}

void arvoreB::percorreEmOrdemAux(pagina* P, int nivel){
    int M = cabecalho.grau;
    for (int i = 0; i < P->num; i++) {
        // se a página não é folha, imprima os dados da página filha i
        // antes de imprimir a chave i
        if (P->folha == 0) {
            pagina filha(M);
            lerPagArquivo(&filha, P->posFilhas[i]);
            percorreEmOrdemAux(&filha, nivel+1); 
        }
        cout << P->objetos[i].chave1 << '/' << nivel << endl;
        P->objetos[i].imprime();
    }
    // Imprima os dados do último filho
    if (P->folha == 0) {
        pagina filha(M);
        lerPagArquivo(&filha, P->posFilhas[P->num]);
        percorreEmOrdemAux(&filha, nivel+1); 
    }
    
}


int main() {
    // 1 34 23 1 200 344 222 12 8 2221 12 9 1 3 4 5 6 6 -1
    arvoreB A("arq1.dat", 3);
    int n;
    cin >> n;
    
    while (n != -1) {
        Dado d;
        d.geraAletorio(n);
        A.insere(d);
        //A.percorreEmOrdem();        
        cin >> n;
    }

    A.percorreEmOrdem();
     

    
    return 0;
}
