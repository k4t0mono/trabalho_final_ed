// class.hpp


#ifndef POKEMON
#define POKEMON 0

struct Pokemon {
  int id;
  char[50] name;
  char[10] type1;
  char[10] type2;
  char[200] desc;
};

struct Block {
  Pokemon data[];
  int n_data;

};

#endif
