// aux.cpp

#include <iostream>
#include <fstream>
#include "aux.hpp"

// ----------------------------------------------------------------------------
// Struct Pokemon
// ----------------------------------------------------------------------------

std::istream& operator>>(std::istream& is, Pokemon& pk) {
  std::cout << "id: " << '\n';
  is >> pk.id;

  is.clear();
  is.ignore();
  std::cout << "nome: " << '\n';
  // is.getline(pk.nome, 51);
  is >> pk.nome;

  is.clear();
  is.ignore();
  std::cout << "tipo1: " << '\n';
  // is.getline(pk.tipo1, 51);
  is >> pk.tipo1;

  is.clear();
  is.ignore();
  std::cout << "tipo2: " << '\n';
  // is.getline(pk.tipo2, 51);
  is >> pk.tipo2;

  std::cout << "altura: " << '\n';
  is >> pk.altura;
  std::cout << "peso: " << '\n';
  is >> pk.peso;

  std::cout << '\n';
  return is;
}

// ----------------------------------------------------------------------------
// Classe Bloco
// ----------------------------------------------------------------------------

Bloco::Bloco() {
  // dados[TAM_BLOCO_SEQUENCE_SET] = NULL;
  this->numPokemons = 0;
  this->prox = -1;
}

// ----------------------------------------------------------------------------
//  Classe Node
// ----------------------------------------------------------------------------

Node::Node(bool folhaP) {
  this->numIndices = 0;
  this->folha = folhaP;
  this->pai = nullptr;

  for(int i = 0; i < ORDEM-1; ++i) {
    this->blocos[i] = -1;
  }

  if(folhaP) {
    this->filhos = nullptr;
  } else {
    this->filhos = new Node*[ORDEM];
  }
}

Node::~Node() {
  delete[] this->filhos;
  pai = nullptr;
}

// ----------------------------------------------------------------------------
//  Classe BPlus
// ----------------------------------------------------------------------------

BPlus::BPlus(std::string nome) {
  this->nomeArquivo = nome;
  std::ifstream fileIn(nome.c_str(), std::ios::binary);

  // Se o arquivo existe
  if(fileIn) {
    // Ler o cabeçalho
    fileIn.read((char*)(&cabecalho), sizeof(CabecalhoArquivo));
    // Se não existir blocos
    if(!this->cabecalho.nroBlocos) {
      raiz = nullptr;
    }
    // Se não
    // } else {
    //   // Criar index
    //   // this->criarIndex();
    //
    //   // Ler index
    //   // this->lerIndex();
    // }

  // O arquivo não existe
  } else {
    raiz = nullptr;
    std::ofstream fileOut(nome.c_str(), std::ios::binary);
    this->cabecalho = {0, -1, -1};
    fileOut.write((char*)(&this->cabecalho), sizeof(CabecalhoArquivo));
    // Bloco b;
    // b.prox = 3;
    // fileOut.write((char*)(&b), sizeof(Bloco));
    fileOut.close();
  }

  fileIn.close();
}

BPlus::~BPlus() {
  delete raiz;
}

void BPlus::inserir(Pokemon pkmn) {
  std::cerr << "Inserido Pokemon" << '\n';
  // Se a raiz é nula
  if(!this->raiz) {
    std::cerr << "Sem itens, criando a raiz" << '\n';
    // Criar uma nova raiz
    this->raiz = new Node(true);
    // Armazenar na raiz um numero arbritario
    ++this->raiz->numIndices;
    this->raiz->indice[0] = 42;
  }

  // Achar o nó q aponta para o bloco a ser inserido
  std::cerr << '\n';
  std::cerr << "Procurando onde inserir" << '\n';
  Node* atual = this->raiz;
  std::cerr << "> atual = raiz" << '\n';
  while(!atual->folha) {
    int i = atual->numIndices-1;
    std::cerr << "> atual não é folha" << '\n';
    for(; i >= -1 and atual->indice[i] > pkmn.id; --i);
    atual = atual->filhos[i+1];
    std::cerr << "Indo para o filho " << i+1 << '\n';
  }
  std::cerr << "Achei o nó folha certo" << "\n\n";

  std::cerr << "Procurando em qual bloco inserir" << '\n';
  // Descubrir a posição do bloco
  int posBloco = atual->numIndices-1;
  std::cerr << "> posBloco = " << posBloco << '\n';
  for(; posBloco >= -1 and atual->indice[posBloco] > pkmn.id; --posBloco);
  // O bloco está em atual->indice[posBloco+1]
  ++posBloco;
  std::cerr << "O bloco está em blocos[" << posBloco << "]\n";

  // Se o bloco não existe
  if(atual->blocos[posBloco] == -1) {
    std::cerr << "O bloco não exite" << '\n';
    atual->blocos[posBloco] = this->criarBloco();
    std::cerr << "O novo bloco está em " << atual->blocos[posBloco] << '\n';
  }

  // Ler bloco
  std::cerr << "Lendo o bloco de RNN " << atual->blocos[posBloco] << '\n';
  Bloco bl;
  std::fstream fl(this->nomeArquivo.c_str(), std::ios::in | std::ios::out | std::ios::binary);
  fl.read((char*)(&bl), (sizeof(CabecalhoArquivo)+sizeof(Bloco)*posBloco));

  // Inserir pkmn ordenado no bloco
  std::cerr << "N de Pokemons no bloco: " << bl.numPokemons << '\n';
  int i = bl.numPokemons-1;
  std::cerr << "> i: " << i << '\n';
  for(; i > -1 and bl.dados[i].id > pkmn.id; --i) {
    std::cerr << "> Copiando de " << i+1 << " para " << i << '\n';
    bl.dados[i+1] = bl.dados[i];
  }
  std::cerr << "Vou inserir em " << i+1 << '\n';
  bl.dados[i+1] = pkmn;

  // Incrementa a quantidade de Pokemons
  ++bl.numPokemons;
  // Se numPokemons > TAM_BLOCO_SEQUENCE_SET
  if(bl.numPokemons > TAM_BLOCO_SEQUENCE_SET) {
    // Dividir bloco
    this->dividirBloco(atual, posBloco);
  }
  // Escrever no bloco
  fl.write((char*)(&bl), (sizeof(CabecalhoArquivo)+sizeof(Bloco)*posBloco));
  // Atualizar cabecalho
  fl.seekp(0);
  fl.write((char*)(&this->cabecalho), sizeof(CabecalhoArquivo));
  std::cerr << '\n';
}

void BPlus::dividirNode(Node* nd) {
  // Criar novo Node 'y'
  // Copiar as ultimas ORDEM-1 chaves de nd para y
  // Se nd for raiz
    // Criar vetor pai e tornar nova raiz
  // Subir a chave do meio de nd
  // Ajustar ponteiros
  // Ajustar tamanhos
}

void BPlus::dividirBloco(Node* pai, int pos) {
  // Bloco x é o bloco na poisição rrn
  // Criar bloco 'y'
  // Copiar os ultimos TAM_BLOCO_SEQUENCE_SET-1
  // objetos de x para y
  // Subir a chave do elemento do meio de x
  // Ajustar ponteiros
  // Ajustar tamanhos
  // Escrever blocos
}

int BPlus::criarBloco() {
  std::cerr << "Criando um novo bloco" << '\n';
  // Abrir o arquivo
  std::fstream arquivo(this->nomeArquivo.c_str(), std::ios::in | std::ios::out | std::ios::binary);
  // Se houver bloco invalido
  if(this->cabecalho.posBlocoInvalida > -1 ) {
    std::cerr << "Tenho um bloco invalido" << '\n';
    // Armazenar o rrn do bloco
    int i = this->cabecalho.posBlocoInvalida;

    // Verificar se o bloco aponta para outro bloco invalido
    Bloco tmp;
    // Pular o cabecalho
    arquivo.seekg(sizeof(CabecalhoArquivo)+sizeof(Bloco)*i);
    // Ler o bloco
    arquivo.read((char*)(&tmp), sizeof(Bloco));
    if(tmp.prox > -1) {
      // Fazer o cabecalho apontar para ele
      this->cabecalho.posBlocoInvalida = tmp.prox;
    }

    // Se não tem raiz
    if(!this->cabecalho.nroBlocos) {
      std::cerr << "Não tenho blocos" << '\n';
      // Cabecalho aponta para ela
      this->cabecalho.posRaiz = i;
    }

    // Atualizar o cabecalho
    ++this->cabecalho.nroBlocos;
    arquivo.write((char*)(&this->cabecalho), sizeof(CabecalhoArquivo));
    arquivo.close();
    // retornar o RRN do invalido
    std::cerr << '\n';
    return i;
  } else {
    std::cerr << "Não tenho blocos invalidos" << '\n';
    // Alocar um bloco vazio;
    Bloco b;
    b.numPokemons = 0;
    b.prox = -1;
    // Escrever o bloco no final do aqruivo
    long unsigned int pos = sizeof(CabecalhoArquivo) + this->cabecalho.nroBlocos*sizeof(Bloco);
    arquivo.seekp(pos);
    arquivo.write((char*)(&b), sizeof(Bloco));

    // Se não tem raiz
    if(!this->cabecalho.nroBlocos) {
      std::cerr << "Não tem blocos no arquivo" << '\n';
      // Cabecalho aponta para ela
      this->cabecalho.posRaiz = 0;
    }

    // Atualizar cabecalho
    ++this->cabecalho.nroBlocos;
    // Escrever cabecalho
    arquivo.seekp(0);
    arquivo.write((char*)(&this->cabecalho), sizeof(CabecalhoArquivo));
    arquivo.close();
    // Retornar a posição no arquivo
    std::cerr << '\n';
    return this->cabecalho.nroBlocos-1;
  }
}
