// aux.hpp

#ifndef ASH
#define ASH 0

#include <iostream>

const int TAM_BLOCO_SEQUENCE_SET = 4;
const int ORDEM = 5;

struct Pokemon {
  int id;
  char nome[51];  // nome do pokemon
  char tipo1[11]; // tipo principal do pokemon
  char tipo2[11]; // tipo secundario do pokemon
  float altura;   // altura do pokemon
  float peso;     // peso do pokemon

  friend std::istream& operator>>(std::istream& is, Pokemon& pk);
  friend std::ostream& operator<<(std::ostream& os, Pokemon& pk);
};

struct Bloco {
  int numPokemons; // Número de pokemons no bloco
  int prox; // RRN do próximo bloco
  Pokemon dados[TAM_BLOCO_SEQUENCE_SET]; // Objetos no bloco

  Bloco();
};

class Node {
  private:
    int indice[ORDEM-1]; // valores que separam os blocos
    bool folha; // se é folha ou não da árvore-B
    Node** filhos; //ponteiros para os filhos (se nó interno)
    Node* pai;
    int blocos[ORDEM]; //RRNs dos blocos (se for nó folha)
    int numIndices; //número de chaves no nó

  public:
    Node(bool folhaP = false);
    ~Node();

  friend class BPlus;
};

struct CabecalhoArquivo {
  int nroBlocos;
  int posRaiz;
  int posBlocoInvalida;
};

class BPlus {
  private:
    CabecalhoArquivo cabecalho;
    std::string nomeArquivo;
    Node* raiz;

    void dividirNode(Node* nd);
    void dividirBloco(Node* pai, int pos);
    Bloco lerBloco(int rrn);
    int criarBloco();
    Node* buscarNode(Node* nd, int i);

  public:
    BPlus(std::string nome);
    ~BPlus();

    void inserir(Pokemon pkmn);

};

#endif
