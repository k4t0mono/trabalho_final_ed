/*
 * Trabalho final da disciplina Estrutura de Dados
 * Arvore B+ - Pokemons
 * Copyright 2017 by Rafael Yukio Takehara Carvalho & Frederico & Breno
 * Arquivo ".cpp" onde são chamadas as funções e casos de testes
*/

#include <iostream>
#include <fstream>
// #include "pokemao.h"
#include "aux.hpp"

int main() {
  BPlus index("pokemon.dat");

  Pokemon a;
  std::cin >> a;

  index.inserir(a);

  std::ifstream fl("pokemon.dat", std::ios::binary);
  Bloco bl;
  fl.seekg(sizeof(CabecalhoArquivo));
  fl.read((char*)(&bl), sizeof(Bloco));

  std::cout << "n pkmn: " << bl.numPokemons << "\n";
  std::cout << "pkmn[0].id: " << bl.dados[0].id << "\n";

  return 0;
}
