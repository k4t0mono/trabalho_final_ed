/*
 * Trabalho final da disciplina Estrutura de Dados
 * Arvore B+ - Pokemons
 * Copyright 2017 by Rafael Yukio Takehara Carvalho & Frederico & Breno
 * Arquivo ".cpp" onde são declarados os funcionamentos das funções declaradas no ".h"
*/


#include <iostream>
#include <cstring>
#include <sstream>
#include <fstream>
#include "pokemao.h"

using namespace std;


// ~~~~~~~ Excecao

//
Excecao::Excecao(TipoExcecao tipo, int linha) {
    Linha = linha;
    Tipo = tipo;
}
//


//
string Excecao::Msg() {

    stringstream Stream;

    Stream << "Ops, something went wrong: ";

    switch (Tipo) {
        case ArvoreVazia:
            Stream << "Empty tree";
            break;
        case ValorNaoPresente:
            Stream << "The data is not in the tree";
            break;
    }
    Stream << " on line " << Linha << ".";

    return Stream.str();
}
//

// ~~~~~~~ Excecao


// ~~~~~~~


// ~~~~~~~ Pokemon

//
pokemon::pokemon(int idP, char *nomeP, char *tipo1P, char *tipo2P, float alturaP, float pesoP) {
    id = idP;
    strcpy(nome, nomeP);
    strcpy(tipo1, tipo1P);
    strcpy(tipo2, tipo2P);
    altura = alturaP;
    peso = pesoP;
}
//


//
pokemon::~pokemon() {
    peso = altura = id = -1;
    for (int i = 0; i < 50; ++i) {
        nome[i] = '\0';
    }
    for (int j = 0; j < 10; ++j) {
        tipo1[j] = '\0';
        tipo2[j] = '\0';
    }
}
//

// ~~~~~~~ Pokemon


// ~~~~~~~


// ~~~~~~~ Bloco

//
bloco::bloco() {
    dados[TAM_BLOCO_SEQUENCE_SET] = NULL;
    numPokemons = 0;
    prox = -1;
}
//

// ~~~~~~~ Bloco


// ~~~~~~~


// ~~~~~~~ Noh

//
noh::noh(bool folhaP) {
    for (int i = 0; i < ORDEM - 1; ++i) {
        indice[i] = -1;
    }

    folha = folhaP; // se é folha ou não da árvore-B
    if (folhaP) {
        for (int i = 0; i < ORDEM; ++i) {
            blocos[i] = -1;
        }
        filhos = NULL;
    } else {
        filhos = new int[ORDEM]
        for (int i = 0; i < ORDEM; ++i) {
            filhos[i] = NULL;
            blocos[i] = -1;
        }
    }
    numIndices = 0;
}
//


//
noh::~noh() {

    for (int j = 0; j < ORDEM - 1; ++j) {
        indice[j] = -1;
    }

    for (int k = 0; k < ORDEM; ++k) {
        blocos[k] = -1;
    }

    for (int i = 0; i < ORDEM; ++i) {
        delete filhos[i];
    }
    delete[] filhos;

    folha = false;
    numIndices = 0;
}
//

// ~~~~~~~ Noh


// ~~~~~~~


// ~~~~~~~ bPlus

bPlus::bPlus(char* nomeArq) {
    arquivo = new fstream;
    fl->open(&nomeArq, ios::binary | ios::in | ios::out);
}
